package inventory;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;

/**
 * Servlet implementation class Deliver
 */
public class Deliver extends HttpServlet {
	private static final long serialVersionUID = 1L;
     DatabaseCon con1;
     Connection con;
     ResultSet res,res1;
     String name,uname,quant,dept;
         /**
     * @see HttpServlet#HttpServlet()
     */
    public Deliver() {
        super();
        con1=new DatabaseCon();
      con=  con1.getConnection();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String req = request.getParameter("reqid");
		String query;
		try{
		//if (request.getParameter("view") != null) {
			query="select * from request where req_id = '"+req+"'";
			res=con1.getResult(query);
			if(res.next()){
				
				request.setAttribute("reqid", req);
				request.setAttribute("name", res.getString("item_name"));
				//request.setAttribute("code", res.getString("code"));
				request.setAttribute("quantity", res.getString("quantity"));
				request.setAttribute("incharge", res.getString("username"));
				if (request.getParameter("view") != null) {
				RequestDispatcher rd = request.getRequestDispatcher("/deliver.jsp");
				rd.forward(request, response);
			}
		 else if (request.getParameter("issue") != null) {
	    	  Deliver del = new Deliver();
	    	  del.issueItem(req);
	    	  RequestDispatcher rd = request.getRequestDispatcher("/deliver.jsp");
				rd.forward(request, response);
	      }
			}}catch(Exception e){
			e.printStackTrace();}
     
  	}
	
		
	public void issueItem(String req){
		try{
			String query="select * from request where req_id = '"+req+"'";
			res=con1.getResult(query);
			if(res.next()){
	 query="insert into issue (issue_id,item,code,incharge,quantity,location) value(?,?,?,?,?,?)";
		
		PreparedStatement pst=con.prepareStatement(query);
		pst.setString(1,req);  
		pst.setString(2,res.getString("item_name"));
		pst.setString(3,"0012");//res.getString("code"));  
		pst.setString(4, res.getString("username"));  
		pst.setInt(5, Integer.parseInt(res.getString("quantity")));  
		pst.setString(6, res.getString("dept_id"));  
		
		int i = pst.executeUpdate();  
		if(i!=0){  
		System.out.print("added Isue");
		query="delete from request where req_id= '"+req+"'";
		pst=con.prepareStatement(query);
		pst.execute();
		}
		
		else{  
		//return("failed to insert the data");  
		}  }
	}catch(Exception e){
		e.printStackTrace();
	}
	}

}
