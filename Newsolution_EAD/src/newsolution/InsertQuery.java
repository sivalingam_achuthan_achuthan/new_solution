package inventory;

import java.io.IOException;
import java.io.PrintWriter;
//import java.io.PrintWriter;

//import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;
/**
 * Servlet implementation class InsertQuery
 */
public class InsertQuery extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String code,descripn;
	int price,quantity,folio;
	DatabaseCon con;
	ResultSet res;
	Connection con1;
	
	boolean valid = false;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertQuery() {
        super();
       
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		response.setContentType("text/html");
		
		 try{
		        con= new DatabaseCon();
		    con1=    con.getConnection();
		        }catch (Exception e){

		    		PrintWriter out = response.getWriter();  
		    response.setContentType("text/html");  
		    out.println("<script type=\"text/javascript\">");  
		    out.println("var ans = alert('Couldnt connect to Database!!');");
		    out.println("if(!ans){window.location=\"http://localhost:8080/inventoy_management/addpermanent.jsp\"}");
		    out.println("</script>");
		        }
		 
		//get parameters from addpermanent.jsp
		
		try{
			
		code = request.getParameter("code");
		quantity = Integer.parseInt(request.getParameter("quantity"));
		folio =Integer.parseInt(request.getParameter("folio"));
		descripn = request.getParameter("description");
		price = Integer.parseInt(request.getParameter("price"));
		
		
	//out.print(roleId);
		String query="select code from permanent where code= '"+code+"'";
		res=con.getResult(query);
		if(res.next()){
		
			// implement update
			
			PrintWriter out = response.getWriter();  
		    response.setContentType("text/html");  
		    out.println("<script type=\"text/javascript\">");  
		    out.println("var ans = confirm(''Particular item is already in the store ! \n Do you want to Update it?);");
		    out.println("if(ans){window.location=\"http://localhost:8080/inventoy_management/addStatQuantity.jsp\"}");
		    out.println("</script>");
			
			
		}else{
			query="insert into permanent (item_name,code,no_of_item,unit_price,folio_no,description) value(?,?,?,?,?,?)";
			PreparedStatement ps=con1.prepareStatement(query);
			ps.setString(1, code);
			ps.setString(2, code);
			ps.setInt(3, quantity);
			ps.setInt(4, price);
			ps.setInt(5, folio);
			ps.setString(6, descripn);
			
			int i = ps.executeUpdate();  
			if(i!=0){  
				PrintWriter out = response.getWriter();  
			    response.setContentType("text/html");  
			    out.println("<script type=\"text/javascript\">");  
			    out.println("var ans = alert('Sucessfully Added');");
			    out.println("if(!ans){window.location=\"http://localhost:8080/inventoy_management/User/addpermanent.jsp\"}");
			    out.println("</script>"); 
			}
			
			//request.setAttribute("uname", "User name is already used");
			//RequestDispatcher rd = request.getRequestDispatcher("/adduser.jsp");
			//rd.forward(request, response);
		}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
