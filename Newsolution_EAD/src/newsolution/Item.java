package newsolution;

import java.util.List;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;




public class Item {

  public static Entity createOrUpdateItem(String productName, String itemName,String price) {
    
    return null;
  }

 
  public static Iterable<Entity> getAllItems() {
  	Iterable<Entity> entities = Util.listEntities("Item", null, null);
  	return entities;
  }

  
  public static Iterable<Entity> getItem(String itemName) {
  	Iterable<Entity> entities = Util.listEntities("Item", "name", itemName);
  	return entities;
  }

  
  public static Iterable<Entity> getItemsForProduct(String kind, String productName) {
    Key ancestorKey = KeyFactory.createKey("Product", productName);
    return Util.listChildren("Item", ancestorKey);
  }

  
  public static Entity getSingleItem(String itemName) {
    
    return null;
  }
  
  public static String deleteItem(String itemKey)
  {
    return null;
  }
}
