
package newsolution;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.google.appengine.api.datastore.Entity;


@SuppressWarnings("serial")
public class ItemServlet extends BaseServlet {

  private static final Logger logger = Logger.getLogger(ItemServlet.class.getCanonicalName());

  
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {

    super.doGet(req, resp);
    logger.log(Level.INFO, "Obtaining Item listing");
    String searchBy = req.getParameter("item-searchby");
    String searchFor = req.getParameter("q");
    PrintWriter out = resp.getWriter();
    if (searchFor == null || searchFor.equals("")) {
      Iterable<Entity> entities = Item.getAllItems();
      out.println(Util.writeJSON(entities));
    } else if (searchBy == null || searchBy.equals("name")) {
      Iterable<Entity> entities = Item.getItem(searchFor);
      out.println(Util.writeJSON(entities));
    } else if (searchBy != null && searchBy.equals("product")) {
      Iterable<Entity> entities = Item.getItemsForProduct("Item", searchFor);
      out.println(Util.writeJSON(entities));
    }
  }

  
  protected void doPut(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    logger.log(Level.INFO, "Creating Item");
    String itemName = req.getParameter("name");
    String productName = req.getParameter("product");
    String price = req.getParameter("price");
    Item.createOrUpdateItem(productName, itemName, price);
  }

 
  protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    String itemKey = req.getParameter("id");
    PrintWriter out = resp.getWriter();
    try{      
      out.println(Item.deleteItem(itemKey));
    } catch(Exception e) {
      out.println(Util.getErrorMessage(e));
    }      
    
  }

 
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    String action = req.getParameter("action");
    if (action.equalsIgnoreCase("delete")) {
      doDelete(req, resp);
      return;
    } else if (action.equalsIgnoreCase("put")) {
      doPut(req, resp);
      return;
    }
  }
}