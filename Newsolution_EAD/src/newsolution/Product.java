package newsolution;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;

import java.util.List;

public class Product {

 
  public static void createOrUpdateProduct(String name, String description) {
    Entity product = getProduct(name);
  	if (product == null) {
  	  product = new Entity("Product", name);
  	  product.setProperty("description", description);
  	} else {
  	  product.setProperty("description", description);
  	}
  	Util.persistEntity(product);
  }


  public static Iterable<Entity> getAllProducts(String kind) {
    return Util.listEntities(kind, null, null);
  }

 
  public static Entity getProduct(String name) {
  	Key key = KeyFactory.createKey("Product",name);
  	return Util.findEntity(key);
  }


  
  public static List<Entity> getItems(String name) {
  	Query query = new Query();
  	Key parentKey = KeyFactory.createKey("Product", name);
  	query.setAncestor(parentKey);
  	query.addFilter(Entity.KEY_RESERVED_PROPERTY, Query.FilterOperator.GREATER_THAN, parentKey);
  		List<Entity> results = Util.getDatastoreServiceInstance()
  				.prepare(query).asList(FetchOptions.Builder.withDefaults());
  		return results;
	}
  

  public static String deleteProduct(String productKey)
  {
	  Key key = KeyFactory.createKey("Product",productKey);	   
	  
	  List<Entity> items = getItems(productKey);	  
	  if (!items.isEmpty()){
	      return "Cannot delete, as there are items associated with this product.";	      
	    }	    
	  Util.deleteEntity(key);
	  return "Product deleted successfully";
	  
  }
}
